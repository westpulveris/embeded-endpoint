# System generujący dane testowe dla DataHub'a

- [System generujący dane testowe dla DataHub'a](#system-generujący-dane-testowe-dla-datahuba)
  - [Ogólny plan tworzenia projektu:](#ogólny-plan-tworzenia-projektu)
  - [Jak uruchomić skrypt](#jak-uruchomićskrypt)
    - [Przez zmienne środowiskowe](#przez-zmienne-środowiskowe)
    - [Podanie argumentów na wejściu programu](#podanie-argumentów-na-wejściu-programu)
  - [Jakiej postaci jest generowany JSON?](#jakiej-postaci-jest-generowany-json)
  - [Jakiej postaci jest plik konfiguracyjny do wysyłania danych?](#jakiej-postaci-jest-plik-konfiguracyjny-do-wysyłania-danych)
  - [Technologie](#technologie)
  - [Hardware](#hardware)
  - [Linki](#linki)

## Ogólny plan tworzenia projektu:
1. Zbudowanie prototypu testowego czujnika (stworzenie konfiguracji GPIO) i implementacja funkcji pobierających dane z czujników
2. Stworzenie skryptu do generowania do danych mockowanych
- Dane randomowe
- Na podstawie pliku
- Na podstawie funkcji
3. Tworzenie JSON’a na podstawie zgromadzonych danych
4. Wysyłanie danych na Datahub do odpowiedniego endpointa

## Jak uruchomić skrypt
### Przez zmienne środowiskowe
W głównym katalogu należy umieścić plik `.env` z następującymi zmiennymi
```sh
EMBEDED_VPN_CONFIG_PATH=FSLABmod.ovpn
EMBEDED_AUTH_PATH=auth.txt
EMBEDED_API_KEY_TOKEN=ABCDEFGHIJK
EMBEDED_GENERATOR_CONFIG_PATH=configs/example_config.json
```
Jeżeli nie zostaną podane, to program pobierze informacje ze argumentów wejścia skryptu.
### Podanie argumentów na wejściu programu
Na wejściu trzeba podać następujące informacje:
1. Ścieżkę do pliku konfiguracyjnego VPN'a,
2. Ściężkę do pliku autoryzacyjnego VPN'a,
3. Token do autoryzacji danych na platformie Datahub,
4. Ścieżka do pliku konfiguracyjnego.

Przykładowe wywołanie skryptu w konsoli z katalogu głównego projektu:
```sh
$ python3 src/main.py FSLABmod.ovpn auth.txt <YOUR_TOKEN> configs/example_config.json
```

## Jakiej postaci jest generowany JSON?
JSON ma postać struktury opisanej w [tym PDF'ie](./docs/Monitorowanie%20jako%C5%9Bci%20powietrza%20-%20interfejs%20w%20stacjach%20v6.pdf).

## Jakiej postaci jest plik konfiguracyjny do wysyłania danych?
Dokładny schemat JSON'a znajduje się w folderze `docs` w pliku [config.schema.json](./docs/config.schema.json).
Przykładowy plik znajduje się [tutaj](./configs/example_config.json).

## Technologie
* Python 3.9
* [adafruit-circuitpython-dht](https://docs.circuitpython.org/projects/dht/en/3.0.1/)

## Hardware
* [Raspberry Pi 4 model B 4GB](https://www.raspberrypi.com/products/raspberry-pi-4-model-b/)
* [DTH 11 Sensor](https://botland.com.pl/czujniki-multifunkcyjne/9301-czujnik-temperatury-i-wilgotnosci-dht11-50c-5904422372668.html?cd=1564049911&ad=58987843373&kd=&gclid=Cj0KCQjw_4-SBhCgARIsAAlegrUAt7cCYgOHRbtG6EG0UqC38zRN7tyg41iJ_Qpo1AxJB5QsUbAMIyoaAngBEALw_wcB)

## Linki
* [How to connect DTH11 Sensor to RPi](https://www.freva.com/dht11-temperature-and-humidity-sensor-on-raspberry-pi/)
