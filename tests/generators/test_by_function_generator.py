import unittest

from generators.by_function_generator import ByFunctionGenerator


class ByFunctionGeneratorTest(unittest.TestCase):
    def test_with_static_function(self):
        const_value = 2
        static_function_generator = ByFunctionGenerator(start_x=0, end_x=10, step_x=1, function=lambda x: const_value)
        solution_list = [const_value for i in range(11)]

        result_list = [number for number in static_function_generator]

        self.assertEqual(result_list, solution_list)

    def test_x_to_power_3(self):
        x_to_power_3_generator = ByFunctionGenerator(start_x=0, end_x=3, step_x=1, function=lambda x: x ** 3,
                                                     function_multiplier=2)
        solution_list = [0, 2, 16, 54]

        result_list = [number for number in x_to_power_3_generator]

        self.assertEqual(result_list, solution_list)
