from time import sleep
import unittest
from generators.sensors.humidity_sensor_generator import HumiditySensorGenerator
from services.dth11_service import DTH11Service


class TestTemperatureSensorGenerator(unittest.TestCase):
    def test_generating(self):
        dth11_service = DTH11Service()
        ammount_of_values = 10
        time_between_reading_values = 0.1
        humidity_generator = HumiditySensorGenerator(dth11_service, ammount_of_values)

        for i in range(ammount_of_values):
            self.assertTrue(next(humidity_generator) > 0)        # if humidity value is more than 0
            sleep(time_between_reading_values)

        with (self.assertRaises(StopIteration)) as si:
            next(humidity_generator)
        
