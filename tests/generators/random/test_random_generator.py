import unittest

from generators.random.random_generator import RandomGenerator


class RandomGeneratorTest(unittest.TestCase):
    def test_generator(self):
        from_value = 0
        to_value = 10
        amount_of_values = 50
        random_generator = RandomGenerator(from_value, to_value, amount_of_values)

        for number in random_generator:
            self.assertTrue(from_value <= number <= to_value)
