import unittest

from generators.json.json_object_generator import JsonObjectGenerator
from generators.math.linear_generator import LinearGenerator


class testJsonObjectGenerator(unittest.TestCase):
    def test_generating(self):
        json_object_generator = JsonObjectGenerator(
            pressure_iter=LinearGenerator(0, 10, 1),
            temperaute_iter=LinearGenerator(10, 20, 1),
            humidity_iter=LinearGenerator(20, 30, 1)
        )

        parsed_json = next(json_object_generator)

        self.assertTrue(isinstance(parsed_json, str))
        self.assertTrue("\"pressure\": 0" in parsed_json)
        self.assertTrue("\"temperature\": 10" in parsed_json)
        self.assertTrue("\"relativeHumidity\": 20" in parsed_json)
