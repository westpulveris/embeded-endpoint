import math
import unittest

from generators.math.sin_generator import SinGenerator


class SinGeneratorTest(unittest.TestCase):
    def test_values(self):
        epsilon = 10 ** (-15)
        sin_generator = SinGenerator(start_x=0, end_x=2 * math.pi, step_x=math.pi / 2)
        solution_list = [0, 1, 0, -1, 0]

        for solution_number in solution_list:
            self.assertTrue(abs(solution_number - next(sin_generator)) < epsilon)
