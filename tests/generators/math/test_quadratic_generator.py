import unittest

from generators.math.quadratic_generator import QuadraticGenerator


class QuadraticGeneratorTest(unittest.TestCase):
    def test_values(self):
        quadratic_generator = QuadraticGenerator(start_x=-3, end_x=3, step_x=1)
        solution_list = [9, 4, 1, 0, 1, 4, 9]

        result_list = [number for number in quadratic_generator]

        self.assertEqual(result_list, solution_list)
