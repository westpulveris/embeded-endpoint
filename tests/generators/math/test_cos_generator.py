import math
import unittest

from generators.math.cos_generator import CosGenerator


class CosGeneratorTest(unittest.TestCase):
    def test_values(self):
        epsilon = 10 ** (-15)
        cos_generator = CosGenerator(start_x=0, end_x=2 * math.pi, step_x=math.pi / 2)
        solution_list = [1, 0, -1, 0, 1]

        for solution_number in solution_list:
            self.assertTrue(abs(solution_number - next(cos_generator)) < epsilon)
