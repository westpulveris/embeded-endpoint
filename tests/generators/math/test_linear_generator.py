import unittest

from generators.math.linear_generator import LinearGenerator


class LinearGeneratorTest(unittest.TestCase):
    def test_values(self):
        linear_generator = LinearGenerator(start_x=-2, end_x=5, step_x=1)
        solution_list = [-2, -1, 0, 1, 2, 3, 4, 5]

        result_list = [number for number in linear_generator]

        self.assertEqual(result_list, solution_list)

    def test_next_value(self):
        linear_generator = LinearGenerator(start_x=10, end_x=10.5, step_x=0.5)

        self.assertEqual(next(linear_generator), 10)
        self.assertEqual(next(linear_generator), 10.5)

        with (self.assertRaises(StopIteration)) as si:
            next(linear_generator)

    def test_function_multiplier(self):
        linear_generator = LinearGenerator(start_x=1, end_x=2, step_x=1, function_multiplier=3)

        self.assertEqual(next(linear_generator), 3)
        self.assertEqual(next(linear_generator), 6)
