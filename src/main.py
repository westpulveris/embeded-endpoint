import time
from constants import API_KEY_TOKEN, AUTH_PATH, GENERATOR_CONFIG_PATH, VPN_CONFIG_PATH
from rest_api.post_json import ApiPoster
from rest_api.vpn import VPNConnection
from services.config_parser_service import ConfigurationParserSevice

SECONDS = 60


def main():
    VPNConnection(VPN_CONFIG_PATH, AUTH_PATH).vpn_connection_main()
    send_jsons_loop(API_KEY_TOKEN, GENERATOR_CONFIG_PATH)


def send_jsons_loop(api_key_token: str, generator_config: str):
    config_parser_service = ConfigurationParserSevice(generator_config)
    api = ApiPoster(api_key_token, config_parser_service.get_endpoint_no())
    iterable_json = config_parser_service.get_json_iterable()
    time_frequency = config_parser_service.get_time_frequency()

    for json_object in iterable_json:
        api.send_data(json_object)
        print("SENDED:\n" + json_object + "\n")
        time.sleep(time_frequency * SECONDS)


if __name__ == '__main__':
    main()
