import random


class RandomGenerator:
    def __init__(self, from_value: float, to_value: float, amount_of_values: int = float('inf')):
        self.__from_value = from_value
        self.__to_value = to_value
        self.__amount_of_values = amount_of_values
        self.__count = 0

    def __iter__(self):
        return self

    def __next__(self) -> float:
        self.__count += 1
        if self.__count > self.__amount_of_values:
            raise StopIteration
        return random.uniform(self.__from_value, self.__to_value)
