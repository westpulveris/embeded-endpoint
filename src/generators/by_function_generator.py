from typing import Callable


class ByFunctionGenerator:
    def __init__(self, start_x: float, end_x: float, step_x: float, function: Callable[[float], float],
                 function_multiplier: float = 1):
        self.__x = start_x - step_x
        self.__step_x = step_x
        self.__end_x = end_x
        self.__function_multiplier = function_multiplier
        self.__function = function

    def __iter__(self):
        return self

    def __next__(self):
        self.__x += self.__step_x
        if abs(self.__x) > abs(self.__end_x):
            raise StopIteration
        return self.__function_multiplier * self.__function(self.__x)
