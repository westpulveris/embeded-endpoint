from datetime import datetime
import json
from typing import Iterable
from generators.math.linear_generator import LinearGenerator

from generators.random.random_generator import RandomGenerator


class JsonObjectGenerator:
    def __init__(self,
                 pressure_iter: Iterable[float],
                 temperaute_iter: Iterable[float],
                 humidity_iter: Iterable[float],

                 amount_of_values: int = float('inf'),
                 label: str = "AGH Mock Sensor",
                 id: str = "KRK/MOCK/TEST",
                 heater_temp_set_iter: Iterable[float] = RandomGenerator(-1, 1),
                 heater_temp_read_iter: Iterable[float] = RandomGenerator(-1, 1),
                 heater_power_iter: Iterable[float] = RandomGenerator(0, 0),
                 tun0IP: str = "127.0.0.1",
                 location_alt: float = 204.5,
                 location_lat: float = 50.0682572840708,
                 location_lon: float = 19.91256601349027,
                 location_name: str = "Kraków 30-055 okolice D17 AGH",
                 dewpoint_iter: Iterable[float] = RandomGenerator(-10, 10),
                 networkIf: str = "wlan0",
                 verbosity: int = 1,
                 network_addr: str = "127.0.0.1",
                 post_mortem_nb: int = 0,
                 supply_voltage: float = 12.247177124023438,
                 pm1_iter: Iterable[float] = RandomGenerator(0, 100),
                 pm10_iter: Iterable[float] = RandomGenerator(0, 100),
                 p2_5_iter: Iterable[float] = RandomGenerator(0, 100),
                 beyond10_iter: Iterable[int] = RandomGenerator(0, 10),
                 beyond0_3_iter: Iterable[int] = RandomGenerator(0, 1000),
                 beyond0_5_iter: Iterable[int] = RandomGenerator(0, 1000),
                 beyond1_0_iter: Iterable[int] = RandomGenerator(0, 100),
                 beyond2_5_iter: Iterable[int] = RandomGenerator(0, 10),
                 beyond5_0_iter: Iterable[int] = RandomGenerator(0, 10),
                 averange_current_draw_iter: Iterable[float] = RandomGenerator(0, 100)) -> None:
        self.__pressure_iter = pressure_iter
        self.__temperaute_iter = temperaute_iter
        self.__humidity_iter = humidity_iter
        self.__amount_of_values = amount_of_values
        self.__label = label
        self.__id = id
        self.__heater_temp_set_iter = heater_temp_set_iter
        self.__heater_temp_read_iter = heater_temp_read_iter
        self.__heater_power_iter = heater_power_iter
        self.__tun0IP = tun0IP
        self.__location_alt = location_alt
        self.__location_lat = location_lat
        self.__location_lon = location_lon
        self.__location_name = location_name
        self.__dewpoint_iter = dewpoint_iter
        self.__networkIf = networkIf
        self.__verbosity = verbosity
        self.__network_addr = network_addr
        self.__post_mortem_nb = post_mortem_nb
        self.__supply_voltage = supply_voltage
        self.__pm1_iter = pm1_iter
        self.__pm10_iter = pm10_iter
        self.__p2_5_iter = p2_5_iter
        self.__beyond10_iter = beyond10_iter
        self.__beyond0_3_iter = beyond0_3_iter
        self.__beyond0_5_iter = beyond0_5_iter
        self.__beyond1_0_iter = beyond1_0_iter
        self.__beyond2_5_iter = beyond2_5_iter
        self.__beyond5_0_iter = beyond5_0_iter
        self.__averange_current_draw_iter = averange_current_draw_iter
        self.__count = 0

    def __iter__(self):
        return self

    def __next__(self) -> str:
        self.__count += 1
        if self.__count > self.__amount_of_values:
            raise StopIteration
        return self.__create_and_parse_json_object()

    def __create_and_parse_json_object(self) -> str:
        return json.dumps(self.__create_json_object(), indent=4)

    def __create_json_object(self):
        return {
            'label': self.__label,
            'timestamp': self.__get_timestamp(),
            'data': {
                'id': self.__id,
                'heater': {
                    'tempSet': next(self.__heater_temp_set_iter),
                    'tempRead': next(self.__heater_temp_read_iter),
                    'powerLevelAberange': next(self.__heater_power_iter)
                },
                'tun0IP': self.__tun0IP,
                'unsent': False,
                'location': {
                    'alt': self.__location_alt,
                    'lat': self.__location_lat,
                    'lon': self.__location_lon,
                    'locName': self.__location_name
                },
                'envSensor': {
                    'dewPoint': next(self.__dewpoint_iter),
                    'pressure': next(self.__pressure_iter),
                    'temperature': next(self.__temperaute_iter),
                    'relativeHumidity': next(self.__humidity_iter)
                },
                'networkIf': self.__networkIf,
                'verbosity': self.__verbosity,
                'networkAddr': self.__network_addr,
                'postMortemNb': self.__post_mortem_nb,
                'supplyVoltage': self.__supply_voltage,
                'particleConcentrationSensor': {
                    'concentration': {
                        'pm1': next(self.__pm1_iter),
                        'pm10': next(self.__pm10_iter),
                        'pm2_5': next(self.__p2_5_iter)
                    },
                    'particleCount': {
                        'beyond10': next(self.__beyond10_iter),
                        'beyound0_3': next(self.__beyond0_3_iter),
                        'beyound0_5': next(self.__beyond0_5_iter),
                        'beyound1_0': next(self.__beyond1_0_iter),
                        'beyound2_5': next(self.__beyond2_5_iter),
                        'beyound5_0': next(self.__beyond5_0_iter),
                    },
                    'averageCurrentDraw': next(self.__averange_current_draw_iter)
                }
            },
            'created_at': self.__get_timestamp()
        }

    @staticmethod
    def __get_timestamp():
        return datetime.now().isoformat()

if __name__ == '__main__':
    json_object_generator = JsonObjectGenerator(
        pressure_iter=LinearGenerator(0, 10, 1),
        temperaute_iter=LinearGenerator(10, 20, 1),
        humidity_iter=LinearGenerator(20, 30, 1)
    )

    parsed_json = next(json_object_generator)
    print((next(json_object_generator)))