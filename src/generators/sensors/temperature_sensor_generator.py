import math
from generators.by_function_generator import ByFunctionGenerator
from services.dth11_service import DTH11Service


class TemperatureSensorGenerator(ByFunctionGenerator):
    def __init__(self, dth11_serive: DTH11Service, amount_of_values: int = math.inf) -> None:
        super().__init__(start_x=1, end_x=amount_of_values, step_x=1, function=lambda x: dth11_serive.get_temperature())