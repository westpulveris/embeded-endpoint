from generators.by_function_generator import ByFunctionGenerator


class LinearGenerator(ByFunctionGenerator):
    def __init__(self, start_x: float, end_x: float, step_x: float, function_multiplier: float = 1):
        super().__init__(start_x, end_x, step_x, lambda x: x, function_multiplier)
