from math import cos

from generators.by_function_generator import ByFunctionGenerator


class CosGenerator(ByFunctionGenerator):
    def __init__(self, start_x: float, end_x: float, step_x: float, function_multiplier: float = 1):
        super().__init__(start_x, end_x, step_x, lambda x: cos(x), function_multiplier)
