import logging
import time
from typing import Callable
import board
import adafruit_dht
import psutil


class DTH11Service:
    WAIT_TIME = 1.0     # in seconds

    def __init__(self, sensor_pin=board.D4) -> None:      # board.D4 means GPIO 4 Pin in RPi
        self.__check_and_kill__process()
        self.__init_sensor(sensor_pin)

    def __check_and_kill__process(self):
        for proc in psutil.process_iter():
            if proc.name() == 'libgpiod_pulsein' or proc.name() == 'libgpiod_pulsei':
                proc.kill()

    def __init_sensor(self, sensor_pin):
        self.sensor = adafruit_dht.DHT11(sensor_pin)

    def get_temperature(self):
        return self.__get_parameter(lambda: self.sensor.temperature)

    def get_humidity(self):
        return self.__get_parameter(lambda: self.sensor.humidity)

    def __get_parameter(self, get_parameter: Callable[[], float]):
        while True:
            try:
                return get_parameter()
            except RuntimeError as error:
                logging.warning(error)
                time.sleep(self.WAIT_TIME)

    def close_connection(self):
        self.sensor.exit()
