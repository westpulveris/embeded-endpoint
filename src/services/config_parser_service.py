import json
from typing import Iterable
from generators.json.json_object_generator import JsonObjectGenerator
from generators.math.sin_generator import SinGenerator
from generators.math.cos_generator import CosGenerator
from generators.math.linear_generator import LinearGenerator
from generators.math.quadratic_generator import QuadraticGenerator
from generators.random.random_generator import RandomGenerator
from generators.sensors.humidity_sensor_generator import HumiditySensorGenerator
from generators.sensors.temperature_sensor_generator import TemperatureSensorGenerator

from services.dth11_service import DTH11Service

BY_FUNCTION_GENERATORS_NAMES = [
    "SinGenerator", "CosGenerator", "LinearGenerator", "QuadraticGenerator"]


class ConfigurationParserSevice:
    def __init__(self, configuration_path: str) -> None:
        self.dth11_service = DTH11Service()
        self.configuration = self.__read_json_object_from_file(
            configuration_path)

    def get_json_iterable(self):
        data_config = self.configuration["data"]
        heater_config = data_config["heater"]
        location_config = self.configuration["location"]

        return JsonObjectGenerator(
            amount_of_values=self.configuration["sampling_frequency"]["number_of_samples"],
            label=self.configuration["sensor_label"],
            id=self.configuration["sensor_id"],
            location_alt=location_config["alt"],
            location_lat=location_config["lat"],
            location_lon=location_config["lon"],
            location_name=location_config["name"],
            temperaute_iter=self.__make_generator(data_config["temperature"]),
            pressure_iter=self.__make_generator(data_config["pressure"]),
            humidity_iter=self.__make_generator(data_config["humidity"]),
            pm1_iter=self.__make_generator(data_config["pm_1"]),
            p2_5_iter=self.__make_generator(data_config["pm_2_5"]),
            pm10_iter=self.__make_generator(data_config["pm_10"]),
            heater_temp_set_iter=self.__make_generator(
                heater_config["temp_set"]),
            heater_temp_read_iter=self.__make_generator(
                heater_config["temp_read"]),
            heater_power_iter=self.__make_generator(heater_config["power"])
        )

    def get_time_frequency(self):       # time frequency is in minutes
        return self.configuration["sampling_frequency"]["time_frequency"]

    def get_endpoint_no(self) -> int:
        return self.configuration["endpoint_no"]

    def __make_generator(self, generator_config) -> Iterable[float]:
        generator_name = generator_config['generator']

        if generator_name == "TemperatureSensorGenerator":
            return TemperatureSensorGenerator(self.dth11_service)
        elif generator_name == "HumiditySensorGenerator":
            return HumiditySensorGenerator(self.dth11_service)
        elif generator_name == "RandomGenerator":
            return RandomGenerator(from_value=generator_config["from_value"], to_value=generator_config["to_value"])
        elif generator_name in BY_FUNCTION_GENERATORS_NAMES:
            return self.__by_function_factory(generator_name,
                                              start_x=generator_config["start_x"],
                                              step_x=generator_config["step_x"],
                                              end_x=float('inf'),
                                              function_multiplier=generator_config.get("function_multiply", 1))

        return RandomGenerator(from_value=0, to_value=100)

    @staticmethod
    def __read_json_object_from_file(path: str):
        return json.load(open(path))

    @staticmethod
    def __by_function_factory(classname: str, start_x: float, step_x: float, end_x: float, function_multiplier: float):
        cls = globals()[classname]      # gets class by it's name
        return cls(start_x=start_x, step_x=step_x, end_x=end_x, function_multiplier=function_multiplier)
