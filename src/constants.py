import os
import sys
from dotenv import load_dotenv


def get_sys_argv_or_default(index: int):
    return sys.argv[index] if (index < len(sys.argv)) else ""


load_dotenv()   # loads environment variables from .env file

# Load from .env file, or get from sys args
VPN_CONFIG_PATH = os.environ.get(
    "EMBEDED_VPN_CONFIG_PATH", get_sys_argv_or_default(1))
AUTH_PATH = os.environ.get("EMBEDED_AUTH_PATH", get_sys_argv_or_default(2))
API_KEY_TOKEN = os.environ.get(
    "EMBEDED_API_KEY_TOKEN", get_sys_argv_or_default(3))
GENERATOR_CONFIG_PATH = os.environ.get(
    "EMBEDED_GENERATOR_CONFIG_PATH", get_sys_argv_or_default(4))
