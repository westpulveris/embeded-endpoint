import sys

import requests

from generators.json.json_object_generator import JsonObjectGenerator
from generators.math.linear_generator import LinearGenerator

URL_BASE = "https://datahub.ki.agh.edu.pl/pl/datasets/dyplomy-2022/endpoints/"
URL_END = "/data/"


class ApiPoster:
    def __init__(self, api_token: str, endopint_no: int):
        self.url = URL_BASE + str(endopint_no) + URL_END
        self.api_token = api_token

    def send_data(self, json_body):
        headers = {
            'token': self.api_token,
            'Content-type': 'application/json',
        }

        requests.post(self.url, headers=headers, data=json_body)


if __name__ == '__main__':

    # vpn_connection = VPNConnection(sys.argv[1], sys.argv[2])
    # vpn_connection.vpn_connection_main()
    # '/home/westpulveris/Documents/wbudowane/embeded-endpoint/src/FSLABmod.ovpn'
    # '/home/westpulveris/Documents/wbudowane/embeded-endpoint/src/auth.txt'

    json_object_generator = JsonObjectGenerator(
        pressure_iter=LinearGenerator(0, 10, 1),
        temperaute_iter=LinearGenerator(10, 20, 1),
        humidity_iter=LinearGenerator(20, 30, 1)
    )

    parsed_json = next(json_object_generator)
    api = ApiPoster(next(json_object_generator), sys.argv[1], 96)
    api.send_data(next(json_object_generator))
