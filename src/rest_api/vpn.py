from select import select
import string
import subprocess
import os
import signal
from time import sleep

TIME_TO_WAIT_FOR_CONNET = 10


class VPNConnection:
    def __init__(self, config_path: string, auth_path: string):
        if not os.path.exists(config_path) or not os.path.exists(auth_path):
            raise Exception

        self.config_path = config_path
        self.auth_path = auth_path
        self.vpn_process = None
        self.command = 'sudo openvpn --config ' + \
            self.config_path + ' --auth-user-pass ' + self.auth_path

    def connect(self):
        self.vpn_process = subprocess.Popen(
            self.command, shell=True, stdout=subprocess.PIPE, stdin=subprocess.PIPE)

    def kill(self):
        os.killpg(os.getpgid(self.vpn_process.pid), signal.SIGTERM)

    def vpn_connection_main(self):
        if check_vpn_connection() is False:
            print("Starting connection")
            self.connect()
            sleep(TIME_TO_WAIT_FOR_CONNET)
            if check_vpn_connection() is False:
                print("Something went wrong, exiting programm")
                print(self.vpn_process.communicate())
                quit()
        else:
            print("You are already connected to agh vpn")


def parse_response_code(code):
    if code == 200:
        print("Everything worked fine!!")
        return True
    if 299 >= code > 200:
        print("Successful response")
        return True
    if 399 >= code >= 300:
        print("Further action needs to be taken in order to complete the request")
        return False
    if 499 >= code > 400:
        print("The request contains bad syntax or cannot be fulfilled")
        return False
    if 599 >= code > 500:
        print("The server failed to fulfil an apparently valid request")
        return False
    print('code not found')
    return False


def check_vpn_connection():
    command = "ip r show table main | grep tun0 | grep 149.156.97"
    p = subprocess.Popen(command, shell=True,
                         stdout=subprocess.PIPE, stdin=subprocess.PIPE)
    out, err = p.communicate()
    if out == b'':
        print("No vpn connetion fo agh cyfronet found")
        return False
    if err != None:
        print("Error occured while checking vpn connection")
        return False
    return True


if __name__ == '__main__':
    # VPNConnection connection = vpn_connection_main(sys.argv[1], sys.argv[2])
    # '/home/westpulveris/Documents/wbudowane/embeded-endpoint/src/FSLABmod.ovpn'
    # '/home/westpulveris/Documents/wbudowane/embeded-endpoint/src/auth.txt'
    # connection.kill()
    pass
