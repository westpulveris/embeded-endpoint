import json

from PyQt5 import QtCore, QtWidgets
from data_generators import DataGenerators


class DetailsWindow(QtWidgets.QWidget):
    def __init__(self, generator_dict, config_dict):
        super().__init__()
        self.setFixedHeight(500)
        self.setFixedWidth(990)
        self.generator_dict = generator_dict
        self.config_dict = config_dict
        self.setup_ui()

    def setup_ui(self):
        self.setWindowTitle("Detailed generator data")
        self.autoFillBackground()
        self.setStyleSheet("background-color: rgb(238, 238, 236);")
        self.centralwidget = QtWidgets.QWidget(self)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(0, 10, 831, 431))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")

        self.set_generators_layout()
        self.set_submit_button()
        self.verticalLayout.addLayout(self.horizontalLayout_submit)

    def set_generators_layout(self):
        self.horizontal_layouts = {}
        self.main_labels = {}
        self.data_inputs = {}
        for data in self.generator_dict:
            self.horizontal_layouts[data] = QtWidgets.QHBoxLayout()
            self.horizontal_layouts[data].setObjectName("horizontal_layout_" + data)
            self.verticalLayout.addLayout(self.horizontal_layouts[data])
            spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
            self.horizontal_layouts[data].addItem(spacerItem)
            self.main_labels[data] = QtWidgets.QLabel(self.verticalLayoutWidget)
            self.main_labels[data].setObjectName("label_" + data)
            self.main_labels[data].setText(
                self.generator_dict[data]["name"] + " - " + self.generator_dict[data]["generator"])
            self.horizontal_layouts[data].addWidget(self.main_labels[data])

            self.horizontal_layouts[data].setAlignment(self.main_labels[data], QtCore.Qt.AlignCenter)
            self.horizontal_layouts[data].setContentsMargins(10, 0, 0, 0)
            self.horizontal_layouts[data].addItem(spacerItem)
            sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Expanding)
            sizePolicy.setVerticalStretch(0)
            self.main_labels[data].setMaximumHeight(30)

            self.data_inputs[data] = {}
            self.data_inputs[data]["generator"] = self.generator_dict[data]

            match DataGenerators[self.generator_dict[data]["generator"]].value:
                case 1:
                    self.horizontal_layouts[data].addItem(spacerItem)
                    self.data_inputs[data]["from_value_label"] = QtWidgets.QLabel()
                    self.data_inputs[data]["from_value_label"].setText("from:")
                    self.data_inputs[data]["from_value_label"].setObjectName(data + "_from_value_label")
                    self.horizontal_layouts[data].addWidget(self.data_inputs[data]["from_value_label"])

                    self.data_inputs[data]["from_value_input"] = QtWidgets.QTextEdit()
                    sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
                    sizePolicy.setHorizontalStretch(0)
                    sizePolicy.setVerticalStretch(0)
                    sizePolicy.setHeightForWidth(
                        self.data_inputs[data]["from_value_input"].sizePolicy().hasHeightForWidth())
                    self.data_inputs[data]["from_value_input"].setSizePolicy(sizePolicy)
                    self.data_inputs[data]["from_value_input"].setText("0")
                    self.data_inputs[data]["from_value_input"].setMaximumSize(QtCore.QSize(100, 30))
                    self.data_inputs[data]["from_value_input"].setObjectName(data + "_from_value_input")
                    self.horizontal_layouts[data].addWidget(self.data_inputs[data]["from_value_input"])

                    self.horizontal_layouts[data].addItem(spacerItem)
                    self.data_inputs[data]["to_value_label"] = QtWidgets.QLabel()
                    self.data_inputs[data]["to_value_label"].setText("to:")
                    self.data_inputs[data]["to_value_label"].setObjectName(data + "_to_value_label")
                    self.horizontal_layouts[data].addWidget(self.data_inputs[data]["to_value_label"])

                    self.data_inputs[data]["to_value_input"] = QtWidgets.QTextEdit()
                    sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed,
                                                       QtWidgets.QSizePolicy.Fixed)
                    sizePolicy.setHorizontalStretch(0)
                    sizePolicy.setVerticalStretch(0)
                    sizePolicy.setHeightForWidth(
                        self.data_inputs[data]["to_value_input"].sizePolicy().hasHeightForWidth())
                    self.data_inputs[data]["to_value_input"].setSizePolicy(sizePolicy)
                    self.data_inputs[data]["to_value_input"].setText("100")
                    self.data_inputs[data]["to_value_input"].setMaximumSize(QtCore.QSize(100, 30))
                    self.data_inputs[data]["to_value_input"].setObjectName(data + "_to_value_input")
                    self.horizontal_layouts[data].addWidget(self.data_inputs[data]["to_value_input"])

                case 2 | 3 | 4:

                    self.horizontal_layouts[data].addItem(spacerItem)

                    # start
                    self.data_inputs[data]["start_x_value_label"] = QtWidgets.QLabel()
                    self.data_inputs[data]["start_x_value_label"].setText("from:")
                    self.data_inputs[data]["start_x_value_label"].setObjectName(data + "start_x_value_label")
                    self.horizontal_layouts[data].addWidget(self.data_inputs[data]["start_x_value_label"])

                    self.data_inputs[data]["start_x_value_input"] = QtWidgets.QTextEdit()
                    sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed,
                                                       QtWidgets.QSizePolicy.Fixed)
                    sizePolicy.setHorizontalStretch(0)
                    sizePolicy.setVerticalStretch(0)
                    sizePolicy.setHeightForWidth(
                        self.data_inputs[data]["start_x_value_input"].sizePolicy().hasHeightForWidth())
                    self.data_inputs[data]["start_x_value_input"].setSizePolicy(sizePolicy)
                    self.data_inputs[data]["start_x_value_input"].setText("0")
                    self.data_inputs[data]["start_x_value_input"].setMaximumSize(QtCore.QSize(100, 30))
                    self.data_inputs[data]["start_x_value_input"].setObjectName(data + "_start_x_value_input")
                    self.horizontal_layouts[data].addWidget(self.data_inputs[data]["start_x_value_input"])

                    # step
                    self.horizontal_layouts[data].addItem(spacerItem)
                    self.data_inputs[data]["step_x_value_label"] = QtWidgets.QLabel()
                    self.data_inputs[data]["step_x_value_label"].setText("step:")
                    self.data_inputs[data]["step_x_value_label"].setObjectName(data + "_step_x_value_label")
                    self.horizontal_layouts[data].addWidget(self.data_inputs[data]["step_x_value_label"])

                    self.data_inputs[data]["step_x_value_input"] = QtWidgets.QTextEdit()
                    sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed,
                                                       QtWidgets.QSizePolicy.Fixed)
                    sizePolicy.setHorizontalStretch(0)
                    sizePolicy.setVerticalStretch(0)
                    sizePolicy.setHeightForWidth(
                        self.data_inputs[data]["step_x_value_input"].sizePolicy().hasHeightForWidth())
                    self.data_inputs[data]["step_x_value_input"].setSizePolicy(sizePolicy)
                    self.data_inputs[data]["step_x_value_input"].setText("1")
                    self.data_inputs[data]["step_x_value_input"].setMaximumSize(QtCore.QSize(100, 30))
                    self.data_inputs[data]["step_x_value_input"].setObjectName(data + "_step_x_value_input")
                    self.horizontal_layouts[data].addWidget(self.data_inputs[data]["step_x_value_input"])

                    # multiply
                    self.horizontal_layouts[data].addItem(spacerItem)
                    self.data_inputs[data]["multiply_value_label"] = QtWidgets.QLabel()
                    self.data_inputs[data]["multiply_value_label"].setText("multiply:")
                    self.data_inputs[data]["multiply_value_label"].setObjectName(data + "_multiply_value_label")
                    self.horizontal_layouts[data].addWidget(self.data_inputs[data]["multiply_value_label"])

                    self.data_inputs[data]["multiply_value_input"] = QtWidgets.QTextEdit()
                    sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed,
                                                       QtWidgets.QSizePolicy.Fixed)
                    sizePolicy.setHorizontalStretch(0)
                    sizePolicy.setVerticalStretch(0)
                    sizePolicy.setHeightForWidth(
                        self.data_inputs[data]["multiply_value_input"].sizePolicy().hasHeightForWidth())
                    self.data_inputs[data]["multiply_value_input"].setSizePolicy(sizePolicy)
                    self.data_inputs[data]["multiply_value_input"].setText("1")
                    self.data_inputs[data]["multiply_value_input"].setMaximumSize(QtCore.QSize(100, 30))
                    self.data_inputs[data]["multiply_value_input"].setObjectName(data + "_multiply_value_input")
                    self.horizontal_layouts[data].addWidget(self.data_inputs[data]["multiply_value_input"])

                case 5 | 6:
                    pass
            self.horizontal_layouts[data].addItem(spacerItem)

    def set_submit_button(self):
        self.horizontalLayout_submit = QtWidgets.QHBoxLayout()
        self.horizontalLayout_submit.setObjectName("horizontalLayout_submit")
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_submit.addItem(spacerItem2)

        self.submit_button = QtWidgets.QPushButton(self.centralwidget)
        self.submit_button.setObjectName("submit_button")
        self.submit_button.setText("submit")
        self.submit_button.setStyleSheet(
            "background-color: rgb(52, 101, 164); color: rgb(238, 238, 236); font: 75 18pt \"Ubuntu Mono\"; border-radius:5px; padding:10px")
        self.horizontalLayout_submit.addWidget(self.submit_button)

        spacerItem3 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_submit.addItem(spacerItem3)
        self.submit_button.clicked.connect(self.collect_input)

    def collect_input(self):
        print("collecting")
        for data in self.generator_dict:
            match DataGenerators[self.generator_dict[data]["generator"]].value:
                case 1:
                    self.generator_dict[data]["from_value"] = float(
                        self.data_inputs[data]["from_value_input"].toPlainText())
                    self.generator_dict[data]["to_value"] = float(
                        self.data_inputs[data]["to_value_input"].toPlainText())

                case 2 | 3 | 4:
                    self.generator_dict[data]["start_x"] = float(
                        self.data_inputs[data]["start_x_value_input"].toPlainText())
                    self.generator_dict[data]["step_x"] = float(
                        self.data_inputs[data]["step_x_value_input"].toPlainText())
                    self.generator_dict[data]["function_multiply"] = float(
                        self.data_inputs[data]["multiply_value_input"].toPlainText())
            self.generator_dict[data].pop("name")
        self.create_json()

    def create_json(self):
        heater = {"heater": {"temp_set": int(self.config_dict["temp_set"]), "temp_read": self.generator_dict["temp_read"],
                             "power": self.generator_dict["power"]}}
        print(heater)
        self.config_dict.pop("temp_set")
        self.generator_dict.pop("temp_read")
        self.generator_dict.pop("power")
        self.generator_dict.update(heater)
        self.config_dict.update(self.generator_dict)

        with open("config.json", "w") as file:
            json.dump(self.config_dict, file, indent=4)
