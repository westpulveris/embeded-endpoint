import enum


class DataGenerators(enum.Enum):
    RandomGenerator = 1
    CosGenerator = 2
    SinGenerator = 3
    QuadraticGenerator = 4
    HumiditySensorGenerator = 5
    TemperatureSensorGenerator = 6
